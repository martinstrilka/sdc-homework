package cz.martinstrilka.sdchomework;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class PrimeResolverTest {

	PrimeResolver primeResolver;

	@Before
	public void setUp() {
		primeResolver = new PrimeResolver();
	}

	@Test
	public void testIsPrime() {
		assertFalse(primeResolver.isPrime(-1));
		assertFalse(primeResolver.isPrime(0));
		assertFalse(primeResolver.isPrime(1));
		assertTrue(primeResolver.isPrime(2));
		assertTrue(primeResolver.isPrime(3));
		assertTrue(primeResolver.isPrime(5));
		assertTrue(primeResolver.isPrime(7));
		assertTrue(primeResolver.isPrime(11));
		assertTrue(primeResolver.isPrime(13));
		// ...
		assertTrue(primeResolver.isPrime(997));
		assertTrue(primeResolver.isPrime(572647637));

	}

}
