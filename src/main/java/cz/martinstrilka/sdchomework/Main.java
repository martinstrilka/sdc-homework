package cz.martinstrilka.sdchomework;

import org.apache.log4j.Logger;

import cz.martinstrilka.sdchomework.executor.Executor;
import cz.martinstrilka.sdchomework.executor.SingleThreadExecutor;
import cz.martinstrilka.sdchomework.numbersupplier.NumberSupplier;
import cz.martinstrilka.sdchomework.numbersupplier.XlsxNumberSupplier;

/**
 * 
 * @author Martin St��lka
 *
 */
public class Main {

	public static void main(String[] args) {
		String pathToFile = args[0];
		PrimeResolver primeResolver = new PrimeResolver();
		try (NumberSupplier numberSupplier = new XlsxNumberSupplier(pathToFile)) {
			numberSupplier.init();

			Executor executor = new SingleThreadExecutor(numberSupplier, primeResolver);
			executor.execute();

		} catch (Exception e) {
			Logger.getLogger(Main.class).error(e);
		}
	}

}
