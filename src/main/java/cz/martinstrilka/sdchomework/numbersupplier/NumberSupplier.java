package cz.martinstrilka.sdchomework.numbersupplier;

import java.util.Iterator;

/**
 * 
 * @author Martin St��lka
 *
 */
public interface NumberSupplier extends Iterator<Long>, AutoCloseable {

	void init() throws Exception;

}
