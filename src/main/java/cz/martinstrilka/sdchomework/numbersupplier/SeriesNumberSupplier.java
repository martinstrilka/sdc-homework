package cz.martinstrilka.sdchomework.numbersupplier;

import java.util.NoSuchElementException;

/**
 * This supplier will provide numbers from given {@code startNumber} up to
 * {@link Long.MAX_VALUE}
 * 
 * @author Martin St��lka
 *
 */
public class SeriesNumberSupplier implements NumberSupplier {

	private long currentNumber;

	public SeriesNumberSupplier(long startNumber) {
		if (startNumber < 0) {
			throw new IllegalArgumentException("Supplier can provide only positive numbers");
		}
		this.currentNumber = startNumber;
	}

	@Override
	public boolean hasNext() {
		return currentNumber < Long.MAX_VALUE;
	}

	@Override
	public Long next() {
		if (!hasNext()) {
			throw new NoSuchElementException();
		}
		return ++currentNumber;
	}

	@Override
	public void close() {
		// nothing to do
	}

	@Override
	public void init() throws Exception {
		// nothing to do
	}

}
