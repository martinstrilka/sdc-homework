package cz.martinstrilka.sdchomework.numbersupplier;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;

import com.monitorjbl.xlsx.StreamingReader;
import com.monitorjbl.xlsx.exceptions.ReadException;

/**
 * This number supplier streams numbers from the XLSX file specified by path.
 * Invalid numbers (negative or unparsable) are skipped
 * 
 * @author Martin St��lka
 *
 */
public class XlsxNumberSupplier implements NumberSupplier {

	private static final int DATA_SHEET_INDEX = 0;
	private static final int DATA_COLUMN_INDEX = 1;
	private static final int BUFFER_SIZE = 4096;
	private static final int ROW_CACHE_SIZE = 100;

	private String pathToFile;
	private Workbook workbook;
	private Iterator<Row> rowIterator;
	private long nextNumber;

	/**
	 * 
	 * @param pathToFile a path to the XLSX file
	 */
	public XlsxNumberSupplier(String pathToFile) {
		this.pathToFile = pathToFile;
	}

	/**
	 * Open given XLSX file and prepare data rows iterator
	 * 
	 * @throws FileNotFoundException if given file does not exists
	 * @throws ReadException         if there is an issue reading the XLSX file
	 */
	@Override
	public void init() throws FileNotFoundException {
		workbook = StreamingReader.builder() //
				.rowCacheSize(ROW_CACHE_SIZE) //
				.bufferSize(BUFFER_SIZE) //
				.open(new FileInputStream(new File(pathToFile)));
		rowIterator = workbook.getSheetAt(DATA_SHEET_INDEX) //
				.rowIterator();
	}

	@Override
	public Long next() {
		return nextNumber;
	}

	@Override
	public boolean hasNext() {
		while (rowIterator.hasNext()) {
			if (nextNumberIsValid()) {
				return true;
			}
		}
		return false;
	}

	private boolean nextNumberIsValid() {
		String nextValue = rowIterator.next() //
				.getCell(DATA_COLUMN_INDEX)//
				.getStringCellValue();
		try {
			nextNumber = Long.parseUnsignedLong(nextValue);
			return true;
		} catch (NumberFormatException e) {
			Logger.getLogger(getClass()).debug("Skipping invalid value: " + nextValue);
			return false;
		}
	}

	@Override
	public void close() throws IOException {
		if (workbook != null) {
			workbook.close();
		}
	}

}
