package cz.martinstrilka.sdchomework;

/**
 * 
 * @author Martin St��lka
 *
 */
public class PrimeResolver {

	/**
	 * Returns {@code true} if given number is prime, otherwise {@code false}. This
	 * algorithm takes advantage of observation that all primes are in form of
	 * (6k+-1) except for 2 and 3.
	 * 
	 * @param number
	 * @return {@code true} if given number is prime, otherwise {@code false}. All
	 *         numbers less than 2 are not prime
	 */
	public boolean isPrime(long number) {
		if (number <= 3) {
			return number > 1;
		}
		if (number % 2 == 0 || number % 3 == 0) {
			return false;
		}
		return iterateDivisors(number);
	}

	/**
	 * Starts with prime number 5. It is sufficient to iterate only to
	 * sqrt({@code number}).
	 * 
	 * @param number
	 * @return {@code true} if given number is prime, otherwise {@code false}
	 */
	private static boolean iterateDivisors(long number) {
		long divisor = 5;
		long maxDivisor = (long) Math.sqrt(number);
		while (divisor <= maxDivisor) {
			if ((number % divisor == 0) || (number % (divisor + 2) == 0)) {
				return false;
			}
			divisor += 6;
		}
		return true;
	}

}
