package cz.martinstrilka.sdchomework.executor;

/**
 * 
 * @author Martin St��lka
 *
 */
@FunctionalInterface
public interface Executor {

	void execute();
}
