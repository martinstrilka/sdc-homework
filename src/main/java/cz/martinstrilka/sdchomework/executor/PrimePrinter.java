package cz.martinstrilka.sdchomework.executor;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * This printer supports printing prime numbers in preserved order. It tracks
 * processed indices by every thread. It allows printing prime number when all
 * other threads already processed all preceding numbers
 * 
 * @author Martin St��lka
 *
 */
public class PrimePrinter {

	private Map<WorkerThread, Long> processedIndices;

	public PrimePrinter() {
		this.processedIndices = new HashMap<>();
	}

	public synchronized void updateIndex(WorkerThread worker, Long processedIndex) {
		processedIndices.put(worker, processedIndex);
		this.notifyAll();
	}

	public synchronized void printPrime(long primeIndex, long prime) {
		while (!canPrint(primeIndex)) {
			waitUntilIndexUpdate();
		}
		System.out.println(prime);
	}

	private boolean canPrint(long indexToPrint) {
		for (Long index : processedIndices.values()) {
			if (index < indexToPrint) {
				return false;
			}
		}
		return true;
	}

	private void waitUntilIndexUpdate() {
		try {
			this.wait();
		} catch (InterruptedException e) {
			Logger.getLogger(getClass()).error(e);
			Thread.currentThread().interrupt();
		}
	}
}