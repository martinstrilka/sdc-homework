package cz.martinstrilka.sdchomework.executor;

import java.util.concurrent.atomic.AtomicLong;

import cz.martinstrilka.sdchomework.PrimeResolver;
import cz.martinstrilka.sdchomework.numbersupplier.NumberSupplier;

/**
 * This worker enables to execute prime number test in parallel manner.
 * Obtaining numbers as well as printing numbers are synchronized actions
 * 
 * @author Martin St��lka
 *
 */
public class WorkerThread implements Runnable {

	private final NumberSupplier numberSupplier;
	private final PrimeResolver primeResolver;
	private final PrimePrinter printer;
	private AtomicLong numberSupplierIndex;
	private long processingIndex;
	private boolean isFinished;

	public WorkerThread(NumberSupplier numberSupplier, PrimeResolver primeResolver, PrimePrinter printer, AtomicLong numberSupplierIndex) {
		this.numberSupplier = numberSupplier;
		this.primeResolver = primeResolver;
		this.printer = printer;
		this.numberSupplierIndex = numberSupplierIndex;
		this.processingIndex = -1;
		this.isFinished = false;
	}

	@Override
	public void run() {
		while (true) {
			long number = obtainNumber();
			if (isFinished) {
				break;
			}
			if (primeResolver.isPrime(number)) {
				printer.updateIndex(this, processingIndex);
				printer.printPrime(processingIndex, number);
			} else {
				printer.updateIndex(this, processingIndex);
			}
		}
		printer.updateIndex(this, Long.MAX_VALUE);
	}

	private long obtainNumber() {
		synchronized (numberSupplier) {
			if (numberSupplier.hasNext()) {
				processingIndex = numberSupplierIndex.incrementAndGet();
				return numberSupplier.next();
			}
			isFinished = true;
			return 0;
		}
	}

}