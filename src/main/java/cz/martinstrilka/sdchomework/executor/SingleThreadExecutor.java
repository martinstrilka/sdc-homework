package cz.martinstrilka.sdchomework.executor;

import cz.martinstrilka.sdchomework.PrimeResolver;
import cz.martinstrilka.sdchomework.numbersupplier.NumberSupplier;

/**
 * This executor is performs prime number test in single thread
 * 
 * @author Martin St��lka
 *
 */
public class SingleThreadExecutor implements Executor {

	private final NumberSupplier numberSupplier;
	private final PrimeResolver primeResolver;

	public SingleThreadExecutor(NumberSupplier numberSupplier, PrimeResolver primeResolver) {
		this.numberSupplier = numberSupplier;
		this.primeResolver = primeResolver;
	}

	@Override
	public void execute() {
		while (numberSupplier.hasNext()) {
			Long number = numberSupplier.next();
			if (primeResolver.isPrime(number)) {
				System.out.println(number);
			}
		}
	}

}
