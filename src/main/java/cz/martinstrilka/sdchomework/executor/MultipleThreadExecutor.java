package cz.martinstrilka.sdchomework.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

import cz.martinstrilka.sdchomework.PrimeResolver;
import cz.martinstrilka.sdchomework.numbersupplier.NumberSupplier;

/**
 * This executor is able to perform prime number test in parallel fashion. Since
 * there is quite a lot of synchronization this executor is suitable only for
 * large numbers approximately greater than 10^9
 * 
 * @author Martin St��lka
 *
 */
public class MultipleThreadExecutor implements Executor {

	private static final int THREADS = 4;
	private final NumberSupplier numberSupplier;
	private final PrimeResolver primeResolver;

	public MultipleThreadExecutor(NumberSupplier numberSupplier, PrimeResolver primeResolver) {
		this.numberSupplier = numberSupplier;
		this.primeResolver = primeResolver;
	}

	@Override
	public void execute() {
		PrimePrinter printer = new PrimePrinter();
		AtomicLong numberSupplierIndex = new AtomicLong(-1L);
		ExecutorService executorService = Executors.newFixedThreadPool(THREADS);
		for (int i = 0; i < THREADS; i++) {
			executorService.execute(new WorkerThread(numberSupplier, primeResolver, printer, numberSupplierIndex));
		}
		executorService.shutdown();
	}
}
